package com.thematrixmc.bungeebans;

import com.thematrixmc.thematrixbungeeapi.Messages;
import com.thematrixmc.thematrixbungeeapi.ranks.Ranks;
import java.sql.SQLException;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;

public class UnbanCommand extends Command{
    
    public UnbanCommand(){
        super("unban", "bungee.command.unban");
    }

    @Override
    public void execute(CommandSender sender, String[] args) {
        ProxiedPlayer player = (ProxiedPlayer) sender;
        if(args.length != 1){
            player.sendMessage(Messages.TAG + "§cIncorrect syntax. Please use: /unban <player>");
        }else{
            ProxiedPlayer target = ProxyServer.getInstance().getPlayer(args[0]);
            try {
                if(!BansAPI.getIfBanned(target.getName())){
                    player.sendMessage(Messages.TAG + "§cPlayer isn't banned!");
                }else{
                    BansAPI.setUnbanned(target);
                    BansAPI.getBanList().remove(target.getName());
                    Core.banReasons.remove(target.getName());
                    player.sendMessage(Messages.TAG + "§6Player " + Ranks.getRankColour(target) + target.getName() + " unbanned.");
                    for(ProxiedPlayer all : ProxyServer.getInstance().getPlayers()){
                        if(Ranks.isStaff(all)){
                            all.sendMessage("§4§lSTAFF §8\u275a §cPlayer" + Ranks.getRankColour(target) + target.getName() + " was unbanned by " + Ranks.getRankColour(player) + player.getName() + ".");
                        }
                    }
                }
            } catch (SQLException ex) {
                System.out.println("Couldn't establish a SQL connection due to a" + ex.toString());
            }
        }
    }

}
