package com.thematrixmc.bungeebans;

import com.thematrixmc.thematrixbungeeapi.Messages;
import com.thematrixmc.thematrixbungeeapi.ranks.Ranks;
import java.sql.SQLException;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;

public class BanCommand extends Command {

    public BanCommand() {
        super("ban", "bungee.command.ban", "permban");
    }

    @Override
    public void execute(CommandSender sender, String[] args) {
        ProxiedPlayer player = (ProxiedPlayer) sender;
        if (args.length < 2) {
            player.sendMessage(Messages.TAG + "§cIncorrect syntax. Please use: /ban <player> <reason> OR /permban <player> <reason>");
        } else {
            ProxiedPlayer target = ProxyServer.getInstance().getPlayer(args[0]);
            try {
                if (BansAPI.getIfBanned(target.getName())) {
             
                } else {
                    String msg = "";
                    for (int i = 1; i < args.length; i++) {
                        String arg = args[i] + " ";
                        msg = msg + arg;
                    }

                    BansAPI.setBanned(player, target, true, msg);
                    BansAPI.getBanList().put(target.getName(), Long.MAX_VALUE);
                    BansAPI.addBanToList(target.getName());
                    
                    Core.banReasons.put(target.getName(), msg);

                    target.disconnect(
                            "§cYou have been banned from TheMatrix Network. \n"
                            + "§cYou have been banned for §6" + msg + "§c. \n"
                            + "§cYour ban will §4never §cexpire. \n"
                            + "§cAppeal your ban on our Teamspeak: §6matrixmc.eu§c.");
                    
                    for(ProxiedPlayer all : ProxyServer.getInstance().getPlayers()){
                        if(Ranks.isStaff(all)){
                            all.sendMessage("§4§lSTAFF §8\u275a §cPlayer" + Ranks.getRankColour(target) + target.getName() + " §cwas banned by " + Ranks.getRankColour(player) + player.getName() + "§4 forever§c. Reason for ban was: §6" + BansAPI.getReason(player));
                        }
                    }
                }
            } catch (SQLException ex) {
                System.out.println("Failed to start SQL connection due to a " + ex.toString());
            }
        }
    }

}
