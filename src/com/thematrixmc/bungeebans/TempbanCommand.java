package com.thematrixmc.bungeebans;

import com.thematrixmc.thematrixbungeeapi.Messages;
import com.thematrixmc.thematrixbungeeapi.ranks.Ranks;
import java.sql.SQLException;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;

public class TempbanCommand extends Command {

    public TempbanCommand() {
        super("tempban", "bungee.command.tempban");
    }

    @Override
    public void execute(CommandSender sender, String[] args) {
        ProxiedPlayer player = (ProxiedPlayer) sender;

        if (args.length <= 2) {
            player.sendMessage(Messages.TAG + "§cIncorrect syntax. Please use: /tempban <player> <timeAndUnit> <reason>");
        } else {
            ProxiedPlayer target = ProxyServer.getInstance().getPlayer(args[0]);
            if (target == null) {
                player.sendMessage(Messages.TAG + "§cPlayer not found.");
            } else {
                if (args[1].length() == 2) {
                    int time = Integer.parseInt(args[1].substring(0, 0));
                    String unit = args[1].substring(1);

                    long endOfBan = System.currentTimeMillis() + BanUnit.getTicks(unit, time);
                    long now = System.currentTimeMillis();
                    long diff = endOfBan - now;

                    if (diff > 0) {
                        try {
                            if (BansAPI.getIfBanned(target.getName())) {
                                player.sendMessage(Messages.TAG + "§cPlayer is already banned!");
                            } else {

                                String msg = "";
                                for (int i = 2; i < args.length; i++) {
                                    String arg = args[i] + " ";
                                    msg = msg + arg;
                                }

                                BansAPI.setBanned(player, target, true, msg);
                                BansAPI.getBanList().put(target.getName(), diff);
                                BansAPI.addBanToList(target.getName());
                                
                                Core.banReasons.put(target.getName(), msg);

                                target.disconnect(
                                        "§cYou have been banned from TheMatrix Network. \n"
                                        + "§cYou have been banned for §6" + msg + "§c. \n"
                                        + "§cYour ban will expire in: §6" + BansAPI.getTimeLeft(diff) + "§c. \n"
                                        + "§cAppeal your ban on our Teamspeak: §6matrixmc.eu§c.");
                            }
                        } catch (SQLException ex) {
                            player.sendMessage(Messages.TAG + "§cFailed to connect to MySQL database. Cannot ban.");
                        }

                    } else {
                        player.sendMessage(Messages.TAG + "§cAn error occurred. Please try again.");
                    }

                } else if (args[1].length() == 3) {
                    int time = Integer.parseInt(args[1].substring(0, 1));
                    String unit = args[1].substring(2);
                    long endOfBan = System.currentTimeMillis() + BanUnit.getTicks(unit, time);
                    long now = System.currentTimeMillis();
                    long diff = endOfBan - now;

                    if (diff > 0) {
                        try {
                            if (BansAPI.getIfBanned(target.getName())) {
                                player.sendMessage(Messages.TAG + "§cPlayer is already banned!");
                            } else {
                                String msg = "";
                                for (int i = 2; i < args.length; i++) {
                                    String arg = args[i] + " ";
                                    msg = msg + arg;
                                }

                                BansAPI.setBanned(player, target, true, msg);
                                BansAPI.getBanList().put(target.getName(), diff);
                                BansAPI.addBanToList(target.getName());
                                
                                Core.banReasons.put(target.getName(), msg);
                               
                                target.disconnect(
                                        "§cYou have been banned from TheMatrix Network. \n"
                                        + "§cYou have been banned for §6" + msg + "§c. \n"
                                        + "§cYour ban will expire in: §6" + BansAPI.getTimeLeft(diff) + "§c. \n"
                                        + "§cAppeal your ban on our Teamspeak: §6matrixmc.eu§c.");

                                for (ProxiedPlayer all : ProxyServer.getInstance().getPlayers()) {
                                    if (Ranks.isStaff(all)) {
                                        all.sendMessage("§4§lSTAFF §8\u275a §cPlayer" + Ranks.getRankColour(target) + target.getName() + " §cwas banned by " + Ranks.getRankColour(player) + player.getName() + "§c for §6" + BansAPI.getTimeLeft(diff) + "§c. Reason for ban was: §6" + BansAPI.getReason(player));
                                    }
                                }
                            }
                        } catch (SQLException ex) {
                            player.sendMessage(Messages.TAG + "§cFailed to connect to MySQL database. Cannot ban.");
                        }

                    } else {
                        player.sendMessage(Messages.TAG + "§cAn error occurred due to BungeeCord issues. Sorry.. #BlameAlex");
                    }

                }

            }
        }
    }
}
