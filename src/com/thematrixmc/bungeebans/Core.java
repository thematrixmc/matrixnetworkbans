package com.thematrixmc.bungeebans;

import java.io.File;
import java.sql.SQLException;
import java.util.HashMap;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.event.PostLoginEvent;
import net.md_5.bungee.api.plugin.Listener;
import net.md_5.bungee.api.plugin.Plugin;
import net.md_5.bungee.event.EventHandler;

public class Core extends Plugin implements Listener {

    public static HashMap<String, Long> banned = new HashMap<>();
    public static HashMap<String, Integer> amountOfBans = new HashMap<>();
    public static HashMap<String, String> banReasons = new HashMap<>();
    public ProxyServer server;

    @Override
    public void onEnable() {
        server = ProxyServer.getInstance();

        server.getPluginManager().registerListener(this, this);

        server.getPluginManager().registerCommand(this, new TempbanCommand());
        server.getPluginManager().registerCommand(this, new CheckbanCommand());
        server.getPluginManager().registerCommand(this, new BanCommand());
        server.getPluginManager().registerCommand(this, new UnbanCommand());

        try {
            BansAPI.setBannedViaConsole("Console", "Abody_M", true, "KKK");

            BansAPI.getBanList().put("Abody_M", Long.MAX_VALUE);
            BansAPI.addBanToList("Abody_M");

            Core.banReasons.put("Abody_M", "KKK and Abuse");
        } catch (SQLException ex) {
            System.out.println("Could not automatically ban Abody_M.");
        }

    }

    @Override
    public void onDisable() {
    }

    @EventHandler
    public void onLogin(PostLoginEvent event) {
        if (banned.containsKey(event.getPlayer().getName())) {
            if (banned.get(event.getPlayer().getName()) == Long.MAX_VALUE) {
                event.getPlayer().disconnect("§cYou have been banned from TheMatrix Network. \n"
                        + "§cYou have been banned for §6" + banReasons.get(event.getPlayer().getName()) + "§c. \n"
                        + "§cYour ban will §4never §cexpire. \n"
                        + "§cAppeal your ban on our Teamspeak: §6matrixmc.eu§c.");
            } else {
                event.getPlayer().disconnect("§cYou have been banned from TheMatrix Network. \n"
                        + "§cYou have been banned for §6" + banReasons.get(event.getPlayer().getName()) + "§c. \n"
                        + "§cYour ban will expire in: §6" + BansAPI.getTimeLeft(banned.get(event.getPlayer().getName())) + "§c. \n"
                        + "§cAppeal your ban on our Teamspeak: §6matrixmc.eu§c.");
            }
        }

        banned.put(event.getPlayer().getName(), Long.MIN_VALUE);
    }

}
