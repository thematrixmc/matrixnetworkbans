package com.thematrixmc.bungeebans;

import com.thematrixmc.thematrixbungeeapi.Messages;
import java.sql.SQLException;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;

public class CheckbanCommand extends Command {

    public CheckbanCommand() {
        super("checkban", "bungee.command.checkban", "baninfo");
    }

    @Override
    public void execute(CommandSender sender, String[] args) {
        ProxiedPlayer player = (ProxiedPlayer) sender;
        if (args.length < 1) {
            player.sendMessage(Messages.TAG + "§cIncorrect syntax. Please use: /checkban <player>");
        } else {
            try {
                if (BansAPI.getIfBanned(args[0])) {
                    ProxiedPlayer target = ProxyServer.getInstance().getPlayer(args[0]);
                    player.sendMessage(Messages.TAG + "§8=== §6Ban Info for " + args[0] + " §8===");
                    player.sendMessage(Messages.TAG + "§6Currently banned? §aYes.");
                    player.sendMessage(Messages.TAG + "§6Currently banned for: §c" + BansAPI.getReason(target));
                    long endofban = Core.banned.get(args[0]);
                    if(endofban == Long.MAX_VALUE){
                        player.sendMessage("§6Unban time: §cNever (Permanent ban)");
                    }else{
                        player.sendMessage("§6Unban time: §c" + BansAPI.getTimeLeft(endofban));
                    }
                    player.sendMessage(Messages.TAG + "§6Current amount of bans: §c" + BansAPI.amountOfBans(args[0]));
                    player.sendMessage(Messages.TAG + "§6Current amount of kicks: §c" + BansAPI.amountOfKicks(args[0]));
                    player.sendMessage(Messages.TAG + "§6Panel page for player: §chttp://matrixmc.eu/staff//player?n=" + args[0]);
                    player.sendMessage(Messages.TAG + "§8=== §6Ban Info for " + args[0] + " §8===");
                }else{
                    player.sendMessage(Messages.TAG + "§8=== §6Ban Info for " + args[0] + " §8===");
                    player.sendMessage(Messages.TAG + "§6Currently banned? §cNo.");
                    player.sendMessage(Messages.TAG + "§6Current amount of bans: §c" + BansAPI.amountOfBans(args[0]));
                    player.sendMessage(Messages.TAG + "§6Current amount of kicks: §c" + BansAPI.amountOfKicks(args[0]));
                    player.sendMessage(Messages.TAG + "§6Panel page for player: §chttp://matrixmc.eu/staff/player?n=" + args[0]);
                    player.sendMessage(Messages.TAG + "§8=== §6Ban Info for " + args[0] + " §8===");
                }
            } catch (SQLException ex) {

            }
        }
    }

}
