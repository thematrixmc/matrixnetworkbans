package com.thematrixmc.bungeebans;

import com.thematrixmc.thematrixbungeeapi.TheMatrixBungeeAPI;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashMap;
import java.util.Random;
import net.md_5.bungee.api.connection.ProxiedPlayer;

public class BansAPI {

    public static boolean getIfBanned(String player) throws SQLException {
        Statement statement = TheMatrixBungeeAPI.conn.createStatement();
        ResultSet rs = statement.executeQuery("SELECT * FROM `BungeeBans_Active` WHERE `Banned`='" + player + "';");
        return rs.next();
    }

    public static HashMap<String, Long> getBanList() {
        return Core.banned;
    }

    public static void addBanToList(String player) {
        if (Core.amountOfBans.containsKey(player)) {
            int previous = Core.amountOfBans.get(player);
            Core.amountOfBans.remove(player);
            Core.amountOfBans.put(player, (previous + 1));
        } else {
            Core.amountOfBans.put(player, 1);
        }
    }

    public static int amountOfBans(String player) throws SQLException {
        Statement statement = TheMatrixBungeeAPI.conn.createStatement();
        ResultSet rs = statement.executeQuery("SELECT * FROM `playerInfo` WHERE `Name`='" + player + "';");
        if(!rs.next()){
            return 0;
        }else{
            return rs.getInt("AmountOfBans");
        }
    }

    public static String getReason(ProxiedPlayer player) throws SQLException {
        return Core.banReasons.get(player.getName());
    }

    public static void setBanned(ProxiedPlayer playerBanning, ProxiedPlayer bannedPlayer, boolean successful, String details) throws SQLException {
        Statement statement = TheMatrixBungeeAPI.conn.createStatement();
        boolean banned = getIfBanned(bannedPlayer.getName());
        if (!banned) {
            statement.executeUpdate("INSERT INTO `BungeeBans_Active` (`ID`,`Banner`,`Banned`,`Successful`,`Details`) VALUES ('" + new Random().nextInt(Integer.MAX_VALUE) + "','" + playerBanning.getName() + "', '" + bannedPlayer.getName() + "', '" + successful + "', '" + details + "');");
        } else {
        }
    }

    public static void setBannedViaConsole(String playerBanning, String bannedPlayer, boolean successful, String details) throws SQLException {
        Statement statement = TheMatrixBungeeAPI.conn.createStatement();
        boolean banned = getIfBanned(bannedPlayer);
        if (!banned) {
            statement.executeUpdate("INSERT INTO `BungeeBans_Active` (`ID`,`Banner`,`Banned`,`Successful`,`Details`) VALUES ('" + new Random().nextInt(Integer.MAX_VALUE) + "','" + playerBanning + "', '" + bannedPlayer + "', '" + successful + "', '" + details + "');");
        } else {
        }
    }
    
    public static void setUnbanned(ProxiedPlayer bannedPlayer) throws SQLException {
        Statement statement = TheMatrixBungeeAPI.conn.createStatement();
        ResultSet rs = statement.executeQuery("SELECT * FROM `BungeeBans_Active WHERE `Banned`='" + bannedPlayer.getName() + "';");
        if(rs.next()){
            int id = rs.getInt("ID");
            String banner = rs.getString("banner");
            String banned = bannedPlayer.getName();
            boolean successful = rs.getBoolean("Successful");
            String details = rs.getString("Details");
            
            statement.executeUpdate("DELETE FROM `BungeeBans_Active` WHERE `ID`='" + id + "';");
            statement.executeUpdate("INSERT INTO `BungeeBans_Inactive` (`ID`,`Banner`,`Banned`,`Successful`,`Details`) VALUES ('" + id + "','" + banner + "', '" + banned + "', '" + successful + "', '" + details + "');");
        }else{
            
        }
    }
    
    public static int amountOfKicks(String player) throws SQLException{
        Statement statement = TheMatrixBungeeAPI.conn.createStatement();
        ResultSet rs = statement.executeQuery("SELECT * FROM `playerInfo` WHERE `Name`='" + player + "';");
        if(!rs.next()){
            return 0;
        }else{
            return rs.getInt("AmountOfKicks");
        }
    }
    
    public static String getTimeLeft(long endOfBan) {
        String message = "";

        long now = System.currentTimeMillis();
        long diff = endOfBan - now;
        int seconds = (int) (diff / 1000);

        if (seconds >= 60 * 30 * 60 * 24 * 12) {
            int days = seconds / (60 * 30 * 60 * 24 * 12);
            seconds = seconds % (60 * 30 * 60 * 24 * 12);

            message += days + " Year(s) ";
        }
        if (seconds >= 60 * 30 * 60 * 24) {
            int days = seconds / (60 * 30 * 60 * 24);
            seconds = seconds % (60 * 30 * 60 * 24);

            message += days + " Month(s) ";
        }
        if (seconds >= 60 * 60 * 24 * 7) {
            int days = seconds / (60 * 60 * 24 * 7);
            seconds = seconds % (60 * 60 * 24 * 7);

            message += days + " Week(s) ";
        }
        if (seconds >= 60 * 60 * 24) {
            int days = seconds / (60 * 60 * 24);
            seconds = seconds % (60 * 60 * 24);

            message += days + " Day(s) ";
        }
        if (seconds >= 60 * 60) {
            int hours = seconds / (60 * 60);
            seconds = seconds % (60 * 60);

            message += hours + " Hour(s) ";
        }
        if (seconds >= 60) {
            int min = seconds / 60;
            seconds = seconds % 60;

            message += min + " Minute(s) ";
        }
        if (seconds >= 0) {
            message += seconds + " Second(s) ";
        }

        return message;
    }

}
